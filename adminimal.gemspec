$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "adminimal/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "adminimal"
  s.version     = Adminimal::VERSION
  s.authors     = ["Francis Needham, Giuseppe Salvo"]
  s.email       = ["francis@needham.it"]
  s.homepage    = "http://www.display.xxx"
  s.summary     = "Summary of Adminimal."
  s.description = "Description of Adminimal."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.0.0"
  s.add_dependency 'will_paginate', '~> 3.1'
  s.add_dependency 'ransack', '~> 1.7'
  s.add_dependency 'formtastic', '~> 3.0'
  s.add_dependency "font-awesome-rails", '~> 4.5'
  
  s.add_development_dependency "sqlite3"
end
