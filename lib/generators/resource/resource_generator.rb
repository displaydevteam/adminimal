class ResourceGenerator < Rails::Generators::NamedBase
  source_root File.expand_path("../templates", __FILE__)

  def copy_resource_files
  	directory ".", "app/views/adminimal/#{file_name}"
  end

end