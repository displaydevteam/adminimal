require 'test_helper'

module Adminimal
  class PagesControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    test "should get dashboard" do
      get pages_dashboard_url
      assert_response :success
    end

  end
end
