# Adminimal
An awesome CRUD admin engine for rails

## Usage
In routes put
mount Adminimal::Engine, at: "/admin"

  Adminimal::Engine.routes.draw do
  	resources :admin_resource_snake_case, :another_admin_resource_snake_case
  end


create controllers in folder app/controllers/adminimal/resource_name_snake_case.rb

default controller should look like:


module Adminimal
  
  class ResourcePluralCamelCaseController < DefaultController
  end

end


MODEL API is simple

in your model you can define

self.row_schema ( array with fields to have in the index page)
self.form_schema ( array of hash for every input - first key should be :field => 'field_name', for the rest use formstatic documentation to access the hash values.)


BATCH API

define your batch actions in model then put them in the resource controller

def index
  		super ( because we keep the default behaviour )
  		@batches = ['publish','unpublish'] ( array containing batch actions names defined in model)
end

SEARCH API

define search attributes in model.

search_attributes must be an array of search field with RANSACK METHODS
Belongs to association will be autodetected ( use field_id_eq ) and specify in parent model search_name with selectbox display value


ORDERING

in model define self.row_ordering with plain order clause syntax


---




HIDE FROM MENU

in routes specify resources variable menu: false 

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'adminimal'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install adminimal
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
