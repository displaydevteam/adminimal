require_dependency "adminimal/application_controller"

module Adminimal
  class BatchController < ApplicationController
  protect_from_forgery :except => [:batch, :sort]

    def sort
    model = params[:model].constantize
    ids = params[:ids]
    ids.each_with_index do |m,i|
     news = model.find(m)
     news.position = i
     news.save
    end
    render :text => "Record Updated"
    end

    def batch
    	model = params[:model].constantize
    	action = params[:batch]
    	ids = params[:ids].split(',')
    	model.where(:id => ids).each do |m|
        m.send action
      end
    end
  end
end
