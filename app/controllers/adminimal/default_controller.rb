require_dependency "adminimal/application_controller"

module Adminimal
  
  class DefaultController < ApplicationController

  before_action :set_element, only: [:show, :edit, :update, :destroy]
  before_action :set_attrs, only: [:new, :edit]
  prepend_before_action :set_names

  def set_names
    @model     = controller_name.classify.constantize
    @singular  = controller_name.singularize
    @modelSym  = @singular.to_sym
    @modelPath = controller_name.pluralize
    @human     = @singular.humanize.to_s.capitalize
    @keys      = @model.attribute_names
    @search_attributes = @model.try(:search_attributes) || nil
  end


  def index
    @q = @model.ransack(params[:q])
    if defined? @model.row_ordering
      @resources = @q.result(distinct: true).order(@model.row_ordering).paginate(page: params[:page])
    else
      @resources = @q.result(distinct: true).paginate(page: params[:page])
    end
    if defined? @model.row_schema
      @row_schema = @model.row_schema 
    else
      @row_schema = ['id']
    end
  end

  def show
  end

  def new
    @resource = @model.new
  end

  def edit
  end

  def create
    @resource = @model.new(element_params)

    respond_to do |format|
      if @resource.save
        format.html { redirect_to eval("#{@modelPath}_path"), notice: "#{@human} was successfully created." }
        format.json { render action: 'show', status: :created, location: @element }
      else
        format.html { render action: 'new' }
        format.json { render json: @element.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @resource.update(element_params)
        format.html { redirect_to eval("#{@modelPath}_path"), notice: "#{@human} successfully updated." }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @resource.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @resource.destroy
    respond_to do |format|
      format.html { redirect_to @resource, notice: "#{@human} was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_element
      @resource = @model.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def element_params
      params.require(@modelSym).permit!
    end

    def set_attrs
       if defined? @model.form_schema
          @attrs = @model.form_schema
       else 
          @attrs = []
          @model.attribute_names.each do |a|
            unless a == 'id' || a == 'created_at' || a == 'updated_at'
              fields = { :field => a }
              @attrs << fields
            end
          end
        end
         

    end
end


end





