module Adminimal
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    before_filter :set_nav

    def set_nav
    	@nav = []
    	Adminimal::Engine.routes.routes.each do |r|
    		if r.defaults[:action] == 'index' && r.defaults[:menu] != false
    			@nav << r.name
    		end
    	end
    end
  end
end
