Adminimal::Engine.routes.draw do
  get '/' => 'pages#dashboard'
  post '/batch' => 'batch#batch', as: :batch
  post '/sort' => 'batch#sort', as: :sort
end
